import pygame
from pygame.locals import*
from pygame import mixer
import random

pygame.init()
bg = pygame.image.load('download.png')
bg1=pygame.image.load('bg2.png')
bg2=pygame.image.load('Background.png')
bg3=pygame.image.load('bg3.jpg')
bg4=pygame.image.load('bg4.jpg')
restart_img = pygame.image.load('restart_button.png')
quit_img = pygame.image.load('quit_button.png')
easy_img=pygame.image.load('Easy.png')
medium_img=pygame.image.load('Medium.png')
hard_img=pygame.image.load('Hard.png')
writting=pygame.font.SysFont("Helvetica",20)
BLACK=(0,0,0)
WHITE=(255,255,255)
GREEN=(0,255,0)
RED=(255,0,0)
BLUE=(0,0,255)
PINK=(250,0,250)
yellow = (200,200,0)
bright_yellow = (255,255,0)
neon = (0,200,200)
bright_neon = (0,255,255)
width,height=900,600

screen=pygame.display.set_mode((width,height))
pygame.display.set_caption("PONG")
clock=pygame.time.Clock()
FPS=30
class Striker:
     def __init__(self,posx,posy,width,height,speed,color):
         self.posx=posx
         self.posy=posy
         self.width=width
         self.height=height
         self.speed=speed
         self.color=color
         self.gameRect=pygame.Rect(posx,posy,width,height)
         self.game=pygame.draw.rect(screen, self.color, self.gameRect)

     def display(self):
         self.game=pygame.draw.rect(screen,self.color,self.gameRect)
     def update(self, ychanged):
         self.posy=self.posy + self.speed*ychanged
         if self.posy<=0:
             self.posy=0
         elif self.posy + self.height >=height:
             self.posy=height-self.height
         self.gameRect=(self.posx, self.posy, self.width, self.height)
     def displayScore(self,text,score,x,y,color):
         text= writting.render(text+str(score), True, color)
         textRect= text.get_rect()
         textRect.center=(x,y)
         screen.blit(text,textRect)
     def getRect(self):
         return self.gameRect

class Ball:
    def __init__(self,posx,posy,rad,spd,clr):
        self.posx=posx
        self.posy=posy
        self.rad=rad
        self.spd=spd
        self.clr=clr
        self.xchanged=1
        self.ychanged= -1
        self.ball = pygame.draw.circle(screen, self.clr, (self.posx, self.posy), self.rad)
        self.initial=1
    def display(self):
        self.ball=pygame.draw.circle(screen, self.clr, (self.posx, self.posy), self.rad)
    def update(self):
        self.posx += self.spd*self.xchanged
        self.posy += self.spd*self.ychanged
        if self.posy <= 0 or self.posy >= height:
            self.ychanged *=-1
        if self.posx <=0 and self.initial:
            self.initial=0
            return 1
        elif self.posx>=width and self.initial:
            self.initial=0
            return -1
        else:
            return 0
    def reset(self):
        self.posx=width//2
        self.posy=height//2
        self.xchanged *= -1
        self.initial =1
    def hit(self):
        self.xchanged *=-1
    def getRect(self):
        return self.ball


def checkwinner(player1_score,player2_score):
    return player1_score>=11 or player2_score>=11
def show_intro():
    win1_font = pygame.font.SysFont(None, 100)
    win2_font = pygame.font.Font("freesansbold.ttf", 20)
    win1_text = win1_font.render("PONG GAME", True, PINK)
    win2_text = win2_font.render("press any key to continue", True, WHITE)

    win1_rect = win1_text.get_rect(center=screen.get_rect().center)
    win2_rect = win2_text.get_rect(center=(screen.get_width() // 2, screen.get_height() // 2 + 50))
    balls = [
        Ball(random.randint(100, width - 100),
             random.randint(100, height - 100),
             7, random.randint(3, 7),
             GREEN),
        Ball(random.randint(100, width - 100),
             random.randint(100, height - 100),
             7, random.randint(3, 7),
             GREEN),
        Ball(random.randint(100, width - 100),
             random.randint(100, height - 100),
             7, random.randint(3, 7),
             GREEN)
    ]

    screen.fill(BLACK)
    screen.blit(bg, (0, 0))

    screen.blit(win1_text, win1_rect)
    screen.blit(win2_text, win2_rect)
    pygame.display.update()
    waiting = True
    while waiting:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
            elif event.type == pygame.KEYDOWN:
                waiting = False
                active = True

        screen.fill(BLACK)
        screen.blit(bg, (0, 0))
        screen.blit(win1_text, win1_rect)
        screen.blit(win2_text, win2_rect)
        for ball in balls:
            ball.update()
            ball.display()
        pygame.display.update()
        clock.tick(FPS)
def select_level():
    WHITE = (255, 255, 255)
    BLACK = (0, 0, 0)

    width, height = 900, 600

    screen = pygame.display.set_mode((width, height))
    pygame.display.set_caption("SELECT LEVEL")

    clock = pygame.time.Clock()
    FPS = 30

    font = pygame.font.Font(None, 40)
    levels = ["Easy", "Medium", "Hard"]
    selected_level = None

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                mouse_pos = pygame.mouse.get_pos()
                button_height = height // len(levels)
                for i, level in enumerate(levels):
                    if i * button_height <= mouse_pos[1] < (i + 1) * button_height:
                        selected_level = level
                        return selected_level

        screen.fill(BLACK)
        screen.blit(bg3, (0, 0))
        easy_width, easy_height = easy_img.get_width(), easy_img.get_height()
        medium_width, medium_height = medium_img.get_width(), medium_img.get_height()
        hard_width, hard_height = hard_img.get_width(), hard_img.get_height()
        easy_rect = pygame.Rect((width - easy_width) //2, height //4 - easy_height, easy_width, easy_height)
        medium_rect = pygame.Rect((width - medium_width) // 2, height // 3.85 + medium_height, medium_width, medium_height)
        hard_rect = pygame.Rect((width - hard_width) //2, height //1.7 + hard_height, hard_width, hard_height)
        screen.blit(easy_img, easy_rect)
        screen.blit(medium_img, medium_rect)
        screen.blit(hard_img, hard_rect)
        
        pygame.display.update()
        clock.tick(FPS)
        if selected_level:
            return selected_level

       
def original(selected_level):
    screen = pygame.display.set_mode((width, height))
    pygame.display.set_caption("PLAYING PONG GAME")
    running=True
    player1= Striker(20,0,10,100,10,BLUE)
    player2= Striker(width-30, 0, 10, 100, 10, RED)
    ball_speeds = {
        "Easy": 5,    
        "Medium": 8,
        "Hard": 12
    }
    ball= Ball(width//2, height//2, 7, ball_speeds[selected_level], GREEN)

    players=[player1, player2]
    player1Score, player2Score= 0,0
    player1ychanged, player2ychanged = 0,0

    while running:
        screen.fill(BLACK)
        screen.blit(bg1,(0,0))
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running= False
            if event.type == pygame.KEYDOWN:
                if event .key == pygame.K_UP:
                    player2ychanged= -1
                if event .key == pygame.K_DOWN:
                    player2ychanged= 1
                if event.key == pygame.K_w:
                    player1ychanged = -1
                if event.key == pygame.K_s:
                    player1ychanged = 1
                
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                    player2ychanged =0
                if event.key == pygame.K_w or event.key == pygame.K_s:
                    player1ychanged =0
                
            
        for player in players:
            if pygame.Rect.colliderect(ball.getRect(), player.getRect()):
                ball.hit()
                mixer.init()
                mixer.music.load('punch-140236.mp3')
                mixer.music.set_volume(1.0)
                mixer.music.play( )

        player1.update(player1ychanged)
        player2.update(player2ychanged)
        point = ball.update()

        if point==-1:
            player1Score+=1
        elif point ==1:
            player2Score+=1

        if point:
            ball.reset()

        player1.display()
        player2.display()
        ball.display()

        player1.displayScore("Player1 - ", player1Score ,100,20,WHITE)
        player2.displayScore("Player2 - ", player2Score ,width-100,20,WHITE)
        pygame.display.update()
        clock.tick(FPS)
        if checkwinner(player1Score,player2Score):
            running=False
            winner=""
            if player1Score>=11:
                winner="PLAYER 1 WON"
            else:
                winner="PLAYER 2 WON"
            winnerfont=pygame.font.Font("freesansbold.ttf",40)
            winnerbg=winnerfont.render(winner,True,WHITE)
            winnerrect= winnerbg.get_rect(center=(width // 2, height// 2))
            screen.blit(winnerbg,winnerrect)
            pygame.display.update()
            pygame.time.wait(3000)
            game_over()
        
def show_instructions():
    screen=pygame.display.set_mode((width,height))
    pygame.display.set_caption("INSTRUCTIONS")
    instructions_font = pygame.font.SysFont(None, 30)
    instructions_text = [
        "WELCOME TO PONG GAME!",
        "",
        "1.Both the players have one striker each to hit the ball",
        "2.If a player misses hitting the ball his opponent gets a point",
        "3.The first player to reach 11 points will become the winner",
        "",
        "PLAYER 1 CONTROLS:",
        "W - Move Up",
        "S - Move Down",
        "",
        "PLAYER 2 CONTROLS:",
        "Arrow Up - Move Up",
        "Arrow Down - Move Down",
        "",
        "Press any key to continue..."
    ]
    instructions_rendered = [instructions_font.render(line, True, WHITE) for line in instructions_text]
    
    screen.fill(BLACK)
    screen.blit(bg2, (0, 0))
    for idx, rendered_line in enumerate(instructions_rendered):
        text_rect = rendered_line.get_rect(center=(width // 2, height // 4 + idx * 30))
        screen.blit(rendered_line, text_rect)
    
    pygame.display.update()
    waiting = True
    while waiting:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
            elif event.type == pygame.KEYDOWN:
                waiting = False
          
def game_over():
    
    screen=pygame.display.set_mode((width,height))
    pygame.display.set_caption("GAME OVER")
    screen.fill((0, 0, 0))
    screen.blit(bg4, (0, 0))
        
    restart_width, restart_height = restart_img.get_width(), restart_img.get_height()
    quit_width, quit_height = quit_img.get_width(), quit_img.get_height()
    
    restart_rect = pygame.Rect((width - restart_width) // 2, height // 2 - restart_height, restart_width, restart_height)
    quit_rect = pygame.Rect((width - quit_width) // 2, height // 3 + quit_height, quit_width, quit_height)
    
    screen.blit(restart_img, restart_rect)
    screen.blit(quit_img, quit_rect)
    pygame.display.update()
    
    waiting = True
    while waiting:
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:
                mouse_pos = pygame.mouse.get_pos()
                if restart_rect.collidepoint(mouse_pos):
                    global game_state
                    game_state="original"
                    waiting = False
                elif quit_rect.collidepoint(mouse_pos):
                    pygame.quit()
                    quit()
def main():
    pygame.init()
    global game_state
    game_state = "start_menu"

    while True:
        if game_state == "start_menu":
            show_intro()
            game_state = "level_selection"

        elif game_state == "level_selection":
            selected_level = select_level()
            game_state = "instructions"

        elif game_state == "instructions":
            show_instructions()
            game_state = "original"

        elif game_state == "original":
            original(selected_level)
            game_state = "game_over"

        elif game_state == "game_over":
            game_over()

if __name__ == "__main__":
    main()
    pygame.quit()


